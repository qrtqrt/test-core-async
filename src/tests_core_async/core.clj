(ns tests-core-async.core
  (:require [clojure.core.async :as async]))

(def print-agent (agent nil))

(defn print-save
  [& vs]
  (send print-agent (fn [_] (apply println vs)))
  nil)

(defn rand-milliseconds
  [min-sec max-sec]
  (let [[min-ms max-ms] (map #(* 1000 %) [min-sec max-sec])]
    (+ min-ms (rand-int (- max-ms min-ms)))))

(defn fetch-entity
  "Simulates a synchronuous network call by blocking the current thread
  for some random time and then returning a 'fetch result'."
  [id]
  (Thread/sleep (rand-milliseconds 0 1))
  {:external-id id
   :processed-at (java.util.Date.)
   :internal-id (java.util.UUID/randomUUID)
   :fetched-by-thread (.getName (Thread/currentThread))})

(defn process-chan
  [in-channel handler-fn]
  (let [out-channel (async/chan)]
    (async/thread
      (loop [v (async/<!! in-channel)]
        (if v
          (do
            (async/>!! out-channel (handler-fn v))
            (recur (async/<!! in-channel)))
          (async/close! out-channel))))
    out-channel))

(defn process-async
  [in-channel handler-fn amount-of-threads]
  (let [processing-chans (set (repeatedly amount-of-threads
                                          #(process-chan in-channel handler-fn)))
        out-results-chan (async/chan)]

    (async/thread
      (loop [pending-chans processing-chans]
        (when (seq pending-chans)
          (let [[result c] (async/alts!! (vec pending-chans))]
            (if result
              (do
                (async/>!! out-results-chan result)
                (recur pending-chans))
              (recur (disj pending-chans c))))))
      (async/close! out-results-chan))

    out-results-chan))

(defn pour-seq-in-chan
  [s]
  (let [c (async/chan)]
    (async/thread
      (doseq [v s]
        (async/>!! c v))
      (print-save "pushed all, closing returned chan.")
      (async/close! c))
    c))

(defn main
  [amount-of-values amount-of-threads]
  (let [results-chan (process-async (pour-seq-in-chan (range amount-of-values))
                                    fetch-entity
                                    amount-of-threads)
        thread-names-in-action (atom #{})]
    (loop [v (async/<!! results-chan)]
      (when v
        #_(print-save ((juxt :external-id :fetched-by-thread) v))
        (swap! thread-names-in-action conj (:fetched-by-thread v))
        (recur (async/<!! results-chan))))

    (print-save "all donez")
    (print-save "seen threads AMOUNT:" (count @thread-names-in-action))))
